(import (pict) (ice-9 match))
(define* (main #:optional (args (command-line)))
  (match args
    ((_ source)
     (let ((pict (load source))
           (target (string-append source ".svg")))
       (pict->file pict target)))))
