(import (srfi srfi-1)) ; for iota and list-tabulate

(polyline
 (map cons
      (iota 100 0 10)
      (list-tabulate 100
                     (lambda (n)
                       (+ 100 (* n (expt -1 n)))))))
