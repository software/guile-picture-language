(define triangles (make-list 10 (triangle 40 30)))

(apply hc-append ; see combinators
       (map (lambda (triangle)
              (fill (rotate (colorize triangle (random-color))
                            (random 360))
                    (random-color)))
            triangles))
